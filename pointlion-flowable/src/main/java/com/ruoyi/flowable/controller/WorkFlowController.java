package com.ruoyi.flowable.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.flowable.service.WorkFLowService;
import com.sun.net.httpserver.Authenticator;
import org.flowable.engine.HistoryService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.history.HistoricProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/workflow")
public class WorkFlowController extends BaseController {

    @Autowired
    private WorkFLowService workFLowService;

    /***
     * 清空所有流程信息!!!!!!!!!!!!!!!!!!!!
     * 测试环境使用，正式环境，请勿调用该接口
     */
    @RequestMapping("/deleteAllFlowInstance")
    @ResponseBody
    public AjaxResult deleteAllFlowInstance(){
        workFLowService.deleteAllFlowInstance();
        return success();
    }

    @RequestMapping("deleteFlowInstanceById")
    @ResponseBody
    public AjaxResult deleteFlowInstanceById(String insId){
        workFLowService.deleteFlowInstanceById(insId);
        return success();
    }
}
